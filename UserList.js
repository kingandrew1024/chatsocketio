[{}]

class UserList {

  constructor() {
    /**@type {User[]}*/
    this.users = [];
    this.totalUsers = 0;
  }

  getTotalUsers(){
    return this.totalUsers;
  }

  /**
   *
   * @param user {User}
   * @return {User}
   */
  addUser(user) {
    this.totalUsers = this.users.push(user);
    return user;
  }

  /**
   *
   * @param socketId
   * @return {User}
   */
  removeUser(socketId) {
    var user = this.getUserBySocketId(socketId);
    if (user) {
      this.users = this.users.filter((user) => user.socketId !== socketId);
      this.totalUsers = this.users.length;
    }

    return user;
  }

  /**
   *
   * @param appId
   * @return {number}
   */
  findUserIdxByAppId(appId) {
    var index = -1;
    this.users.some((user, idx) => {
      if (user.appId === appId) {
        index = idx;
        return true;
      }
    });
    return index;
  }

  /**
   *
   * @param appId
   * @return {number}
   */
  findUserIdxBySocketId(socketId) {
    var index = -1;
    this.users.some((user, idx) => {
      if (user.socketId === socketId) {
        index = idx;
        return true;
      }
    });
    return index;
  }

  getMyAppId(socketId){
    var appId = -1;
    this.users.some((user) => {
      if (user.socketId === socketId) {
        return appId = user.appId;
      }
    });
    return appId;
  }

  /**
   * @param socketId
   * @return {User[]}
   */
  getMyUserBySocketId(socketId){
    return this.users.filter((user) => {
      return user.socketId === socketId
    })
  }

  getMyUserByAppId(appId){
    return this.users.filter((user) => {
      return user.appId === appId
    })
  }

  /**
   *
   * @param socketId {string}
   * @return {User}
   */
  getUserBySocketId(socketId) {
    return this.users.filter((user) => user.socketId === socketId)[0]
  }

  /**
   * @param appId {number}
   * @return {User}
   */
  getUserByAppId(appId) {
    return this.users.filter((user) => user.appId === appId)[0]
  }

  /**
   *
   * @param roomId {string}
   * @return {User[]}
   */
  getUsersInRoom(roomId) {
    return this.users.filter((user) => {
      return user.roomId === roomId
    });
  }

  /**
   *
   * @param roomId {string}
   * @return {*[]}
   */
  getUserNamesInRoom(roomId) {
    return this.users.filter((user) => {
      return user.roomId === roomId
    }).map((user) => {
      return user.name
    });
  }

  getAllUsers() {
    return this.users;
  }
}

module.exports = {UserList};
