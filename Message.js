[{}]
const SENDER_TYPE = {
  PRIVATE: 1,
  GROUP: 2
};

class Message {
  /**
   * @param {Object} data
   * @param {string} data.uniqueId
   * @param {number?} data.senderAppId
   * @param {number?} data.groupId
   * @param {number} data.receiverAppId
   * @param {string?} data.text
   * @param {number?} data.fileType
   * @param {string?} data.fileSrc
   * @param {string?} data.fileMimeType
   * @param {*?} data.extraData
   * @param {string}data.createdAt
   */
  constructor(data) {
    //console.log("Build Message with this data", data)
    this.uniqueId = data.uniqueId || (data.senderAppId + ":" + new Date().getTime());
    this.senderAppId = data.senderAppId;
    this.groupId = data.groupId || null;
    this.senderType = data.groupId ? SENDER_TYPE.GROUP : SENDER_TYPE.PRIVATE;
    this.receiverAppId = data.receiverAppId || null;
    this.text = data.text || null;
    this.fileType = data.fileType || null;
    this.fileSrc = data.fileSrc || null;
    this.fileMimeType = data.fileMimeType || null;
    this.extraData = data.extraData || null;
    this.createdAt = data.createdAt;
    this.processedAt = new Date().toISOString();
    this.receivedAt = null;
  }
}


module.exports = {Message};

/**
 * @typedef {Object} Message
 * @property {number} Message.uniqueId
 * @property {number} Message.senderAppId
 * @property {number} Message.groupId
 * @property {number} Message.senderType
 * @property {number} Message.receiverAppId
 * @property {string} Message.text
 * @property {number} Message.fileType
 * @property {string} Message.fileSrc
 * @property {string} Message.fileMimeType
 * @property {string} Message.extraData
 * @property {string} Message.createdAt
 * @property {string} Message.processedAt
 * @property {string} Message.receivedAt
 */