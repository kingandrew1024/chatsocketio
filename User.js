[{}]

class User {
  constructor(appId, socketId, name, avatarUrl, roomId) {
    this.appId = appId;
    this.socketId = socketId;
    this.name = name;
    this.avatar = avatarUrl;
    this.roomId = roomId;
  }
}


module.exports = {User};

/**
 * @typedef {Object} User
 * @property User.appId {number}
 * @property User.socketId {string}
 * @property User.name {string}
 * @property User.avatar {string}
 * @property User.roomId {string}
 */
