"use strict";

var express = require('express');
var app = express();

var path = require('path');
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var port = process.env.PORT || 3000;

//var ipaddr = process.env.VCAP_APP_HOST || '172.31.16.55';

server.listen(port, function () {
  console.log(getLogDate() + 'Server listening at port %d', port);
});

// Routing
app.use(express.static(path.join(__dirname, 'public')));


const {DB} = require('./DBService.js');
const myDB = new DB();

const {User} = require('./User.js');
const {UserList} = require('./UserList.js');
const {Message} = require('./Message.js');

function noop() {
  //empty function :)
}


const NO_USERNAME = 'No username';

const STATUS = {
  //MSG_OUTGOING: 1,//FOR SENDER: when I try to send the message to server
  MSG_ARCHIVED: 2,//FOR SENDER: when server processed msg and saved due to receiver is offline
  MSG_DELIVERING: 3,//FOR SENDER: when server is sending msg to receiver
  //MSG_RECEIVER_SAVED: 4,//FOR SENDER: when receiver got the msg but wasn't in same room
  //MSG_RECEIVER_READ: 5//FOR SENDER: when receiver got the msg and was in same room
};

//Users using the APP (but maybe not in a chatroom)
let userList = new UserList();


function startSocket() {
  io.on('connection', (socket) => {
    if (!socket.handshake.query.USER_APP_ID) {
      socket.on('getUserlist', function (data, cb) {
        (cb || noop)(userList);
      });
      return;
    }


    console.log("----------------------------------------------");
    let userAppId = parseInt(socket.handshake.query.USER_APP_ID);

    let user = new User(
      userAppId,                                      //app id
      socket.id,                                      //socket id
      socket.handshake.query.USER_NAME || NO_USERNAME,//user name
      socket.handshake.query.USER_AVATAR,             //user avatar
      userAppId                                       //default room name
    );

    userList.removeUser(socket.id);
    userList.addUser(user);
    console.log(getLogDate() + "User connected {id: %s, name: '%s'}, total users: %d", user.appId, user.name, userList.totalUsers);


    socket.broadcast.emit('admin:user.connected', {user: user, totalUsers: userList.totalUsers});

    //for all users in same room as me, notify I'm online
    socket.broadcast.to(user.appId).emit('peer.connection:change', {userAppId: user.appId, isOnline: true});

    socket.on('message.private:getPending', messagePrivateGetPending);

    //join user to his/her contact's room
    socket.on('room:join', (userAppId, cb) => {
      var contactIdx = userList.findUserIdxByAppId(userAppId);
      //console.log(getLogDate() + "-----  contact %d has index %d", userAppId, contactIdx);
      socket.join(userAppId, function () {
        //console.log(getLogDate() + "********************* user joined to contact room %s", userAppId);
        (cb || noop)({success: true, data: contactIdx > -1});//is online?
        //_showRooms(socket);
      })
    });

    socket.join(user.appId, function () {
      //console.log(getLogDate() + "********************* user joined to contact room %s", userAppId);
      //console.log(getLogDate() + "Joinig user (%s, id: %d) to his/her own room", user.name, user.appId);
      //_showRooms(socket);
    })

    socket.on('room.private:join', (data, cb) => {
      roomPrivateJoin(socket, data, cb);
    });

    socket.on('room.private:leave', (roomId, cb) => {
      roomPrivateLeave(socket, roomId, cb);
    });

    socket.on('message.private:send', (data, cb) => {
      messagePrivateSend(socket, data, cb);
    });

    socket.on('message.private:receiver-acknowledge', (ackObj, cb) => {
      messagePrivateReceiverAcknowledge(socket, ackObj, cb)
    });

    socket.on('message.pending:saved', messagePendingSaved);

    socket.on('message.pending:cleanReceived', messagePendingCleanReceived);

    socket.on('message.private:getHistoric', messageGetHistoric)

    socket.on('disconnect', () => {
      onClientDisconnects(socket);

      if (socket.handshake.query.USER_APP_ID) {
        socket.broadcast.emit('admin:user.disconnected', {user: user, totalUsers: userList.totalUsers});
      }
    });

    //force client close connection
    socket.on('logout', function (data, cb) {
      (cb || noop)(true);
      socket.disconnect();
    });
  });
}

startSocket();

/**
 *
 * @param {*} socket
 * @param roomData {{roomId:string, peerAppId:number}}
 * @param {function(*)} cb
 */
function roomPrivateJoin(socket, roomData, cb) {
  var myListIndex = userList.findUserIdxBySocketId(socket.id);

  if (myListIndex === -1) {
    (cb || noop)({success: false, data: "Your user instance wasn't found in chat server"});
    console.error(getLogDate() + "Your user instance wasn't found in chat server");
  }


  var myUser = userList.users[myListIndex];

  var peerListIdx = userList.findUserIdxByAppId(roomData.peerAppId);
  var isInSameRoom = peerListIdx > -1 && userList.users[peerListIdx].roomId === roomData.peerAppId;

  socket.join(roomData.peerAppId, function () {
    (cb || noop)({
      success: true,
      data: {isPeerOnLine: peerListIdx > -1, roomId: roomData.peerAppId, isInSameRoom: isInSameRoom}
    });

    //console.log(getLogDate() + "Joining user (%s, id: %d) to room %s", myUser.name, myUser.appId, roomData.peerAppId);

    //now we need to update the in-memory user data
    myUser.roomId = roomData.peerAppId;

    //socket.broadcast.to(myUser.roomId).emit('peer.connection:changed', {userAppId: myUser.appId, status: true});

    socket.broadcast.emit('admin:user.room.changed', myUser);
  });
}

/**
 *
 * @param {*} socket
 * @param roomId
 * @param {function(*)} cb
 */
function roomPrivateLeave(socket, roomId, cb) {
  //console.log(getLogDate() + "User has leaft the room:", roomId);

  var myListIndex = userList.findUserIdxBySocketId(socket.id);

  if (myListIndex === -1) {
    (cb || noop)({success: false, data: "Your user instance wasn't found in chat server"});
    return;
  }

  var myUser = userList.users[myListIndex];

  //set to default room
  userList.users[myListIndex].roomId = userList.users[myListIndex].appId;

  //console.log(getLogDate() + userList.getAllUsers());

  socket.broadcast.to(roomId).emit('room.private:userLeft', {userAppId: myUser.appId});

  socket.broadcast.emit('admin:user.room.changed', myUser);

  (cb || noop)({success: true, data: roomId});
}

let undeliveredMsgList = [];

/**
 *
 * @param {*} socket
 * @param {Object} data
 * @param {string} data.uniqueId
 * @param {number} data.contactId
 * @param {number} data.groupId
 * @param {string} data.text
 * @param {number} data.fileType
 * @param {string} data.fileSrc
 * @param {string} data.fileMimeType
 * @param {*} data.extraData
 * @param {string} data.createdAt
 * @param {function(*)} cb
 */
function messagePrivateSend(socket, data, cb) {
  //console.log(getLogDate() + "--- message.private:send")
  var peerListIdx = userList.findUserIdxByAppId(+data.contactId);

  var myUser = userList.getMyUserBySocketId(socket.id)[0];

  /**@type {Message}*/
  var msg = new Message({
    uniqueId: data.uniqueId,
    senderAppId: myUser.appId,
    groupId: data.groupId,
    receiverAppId: data.contactId,
    text: data.text,
    fileType: data.fileType,
    fileSrc: data.fileSrc,
    fileMimeType: data.fileMimeType,
    extraData: data.extraData,
    createdAt: data.createdAt,
  });
  //console.log(getLogDate() + "----- msg Processed at: %s", msg.processedAt);
  
  myDB.saveMessage(msg)

  //receiver is offline
  if (peerListIdx === -1) {
    //console.log(getLogDate() + "Archiving sent msg because user is offline (not found)");
    myDB.savePendingMsg(msg, function (insertedId) {
      (cb || noop)({
        success: true,
        data: {status: STATUS.MSG_ARCHIVED, uniqueId: msg.uniqueId, processedAt: msg.processedAt}
      });
      //console.log(getLogDate() + "Archiving msg (peer offline) with id %s", insertedId);
    }, function (error) {
      (cb || noop)({success: false, data: error});
      console.error(getLogDate() + "Can't archive message", error);
    });
  }
  else {
    (cb || noop)({
      success: true,
      data: {status: STATUS.MSG_DELIVERING, uniqueId: msg.uniqueId, processedAt: msg.processedAt}
    });

    var peerUser = userList.users[peerListIdx];
    //var isInSameRoom = peerListIdx > -1 && peerUser.roomId === myUser.roomId;

    //console.log(getLogDate() + "Sending msg [uid: %s] to {id: %s, name: %s}", msg.uniqueId, peerUser.appId, peerUser.name);
    //console.log(getLogDate() + "from", myUser);

    var receiverAppId = msg.receiverAppId;
    var tOutId = setTimeout(function () {
      msg.receiverAppId = receiverAppId;
      myDB.savePendingMsg(msg, function () {
        console.warn(getLogDate() + "***** Message archived [uid: %s] due to delivery timeout!!!", msg.uniqueId);
        io.in(myUser.socketId).emit('message.private:archived', {
          uniqueId: msg.uniqueId,
          processedAt: msg.processedAt
        });

        setTimeout(function () {
          _showRooms(socket);
        }, 800)
      }, function (error) {
        console.error(getLogDate() + "Can't archive message [uid: %s]", msg.uniqueId, error);
      });
    }, 10 * 1000);
    undeliveredMsgList.push({timeoutId: tOutId, msgUniqueID: msg.uniqueId});

    delete msg.receivedAt;
    delete msg.receiverAppId;
    socket.to(peerUser.socketId).emit('message.private:new', {
      fromUserAppId: myUser.appId,
      msg: msg,
      senderName: myUser.name
    });
  }
}

/**
 * @param {*} socket
 * @param ackObj {{
 *   senderAppId: number, fromUserAppId:string, status: number, processedAt: string, receivedAt: string, uniqueId:number
 * }}
 * @param {function(*)} cb
 */
function messagePrivateReceiverAcknowledge(socket, ackObj, cb) {
  //console.log(getLogDate() + "message %d ack'ed by user %d", ackObj.uniqueId, ackObj.fromUserAppId);
  undeliveredMsgList.some(function (item, idx) {
    if (ackObj.uniqueId === item.msgUniqueID) {
      clearTimeout(item.timeoutId);
      undeliveredMsgList.splice(idx, 1);
      //console.log(getLogDate() + "Msg [uid: %s] interval cancelled, size: %d", item.msgUniqueID, undeliveredMsgList.length);
    }
  });

  //console.log(getLogDate() + "El mensaje ha sido recibido por el userAppId %d", ackObj.senderAppId, ackObj);
  var senderListIdx = userList.findUserIdxByAppId(ackObj.senderAppId);
  var senderUser = userList.users[senderListIdx];
  if (senderUser) {
    socket.to(senderUser.socketId).emit('message.private:receiver-acknowledged', {
      fromUserAppId: ackObj.fromUserAppId,
      status: ackObj.status,
      processedAt: ackObj.processedAt,
      receivedAt: ackObj.receivedAt,
      uniqueId: ackObj.uniqueId
    });
  }

  (cb || noop)({success: true, data: null});
}

/**
 *
 * @param {{receiverAppId: receiverAppId, receivedAt:receivedAt}} data
 * @param {function(*)} cb
 */
function messagePendingSaved(data, cb) {
  myDB.ackPendingMsgs(data, function (affectedRows) {
    (cb || noop)({success: true, data: affectedRows/*<number>*/});
  }, function (error) {
    (cb || noop)({success: false, data: error});
  })
}

/**
 *
 * @param userAppId
 * @param {function(*)} cb
 */
function messagePendingCleanReceived(userAppId, cb) {
  //console.log(getLogDate() + "Cleaning pending msgs from userAppId %d", userAppId);
  myDB.cleanPendingMsgs(userAppId, function (uniqueIdList) {
    (cb || noop)({success: true, data: uniqueIdList/*<[{uniqueId:number, receivedAt:string}]>*/});
  }, function (error) {
    (cb || noop)({success: false, data: error});
  })
}

/**
 *
 * @param userAppId
 * @param {function(*)} cb
 */
function messageGetHistoric(userAppId, cb) {
  console.log(getLogDate() + "Getting historic msgs from userAppId %d", userAppId);
  myDB.getHistoricMsgsByUserId(userAppId, function (messageList) {
    (cb || noop)({success: true, data: messageList});
  }, function (error) {
    (cb || noop)({success: false, data: error});
  })
}

/**
 *
 * @param {number} userAppId
 * @param {function(*)} cb
 */
function messagePrivateGetPending(userAppId, cb) {
  myDB.getPendingMsgsByUserId(userAppId, function (dbResp) {
    //console.log(getLogDate() + "message.private:pendingCount", dbResp)
    (cb || noop)({success: true, data: dbResp/*<[{any}]>*/});
  }, function (error) {
    (cb || noop)({success: false, data: {error: error}});
    console.error(getLogDate() + "on message.private:pendingCount", error);
  })
}

/**
 *
 * @param {*} socket
 */
function onClientDisconnects(socket) {
  var user = userList.removeUser(socket.id);

  if (user) {
    /*socket.leave(user.roomId, function () {
      console.log(getLogDate() + "Now you have left your own room!")
    });*/

    socket.broadcast.to(user.appId).emit('peer.connection:change', {userAppId: user.appId, isOnline: false});
    console.log(getLogDate() + "User disconnected {id: %d, name: %s} total users %d", user.appId, user.name, userList.totalUsers);
  }
  else
    console.log(getLogDate() + 'The user %s was not found but now is disconnected', socket.id);
}

function _showRooms(socket) {
  //var roomsKeys = Object.keys(socket.adapter.rooms);
  //console.log(getLogDate() + "//-------------------------------------//")
  console.log(getLogDate() + "Current Rooms", socket.adapter.rooms);
}

function _showUsersInRoom(roomId) {
  io.of('/').in(roomId).clients(function (error, clients) {
    console.log(getLogDate() + "Clients", clients);
  })
}

function getLogDate() {
  return "[" + new Date().toLocaleString() + "] io:: "
}


//-------------------------------------------------------------------
app.get('/', (req, res) => {
  /*var resp = `<div>Total users: ${userList.totalUsers}</div>`;
  userList.users.forEach(function (user, idx) {
    resp += `<div>${idx + 1}) ${user.name} [id: ${user.appId}, room: ${user.roomId}]</div>`;
  });*/
  var resp = "Welcome to Chat LMT (users connected "+userList.totalUsers+")";

  res.send(resp);
});