[{}]

var noop = function () {
  //empty function :)
};

var conn;
//------------ MYSQL SECTION --------
var mysql = require('mysql');


//============ END MYSQL SECTION ============

class DB {
  constructor() {
    conn = mysql.createConnection({
      host: "localhost",
      user: "root",
      password: "root",
      database: "chat"
    });
  }

  connect(onSuccess, onError) {
    try {
      conn.connect(function (error) {
        if (error) {
          console.error("Can't connect to socket.io DB", error.sqlMessage);
          (onError || noop)(error.sqlMessage);
        }
        else
          (onSuccess || noop)();
      });
    } catch (e) {
      console.error("Can't connect to socket.io DB");
      (onError || noop)(e.message);
    }
  }

  /**
   *
   * @param {Message} data
   * @param onSuccess
   * @param onError
   */
  savePendingMsg(data, onSuccess, onError) {
    //console.log("Inserting pending message", data);
    let qryStr = 'INSERT INTO pending_messages (uniqueId, senderAppId, groupId, senderType, receiverAppId, text, fileType, fileSrc, fileMimeType, extraData, createdAt, processedAt, receivedAt) VALUES (?)';
    let values = [
      [
        data.uniqueId, data.senderAppId, data.groupId, data.senderType, data.receiverAppId, data.text, data.fileType,
        data.fileSrc, data.fileMimeType, data.extraData, data.createdAt, data.processedAt, data.receivedAt
      ]
    ];

    conn.query(qryStr, values, function (error/*, result*/) {
      if (error) {
        console.error("DB::savePendingMsg: INSERT INTO pending_messages", error.sqlMessage);
        (onError || noop)(error.sqlMessage);
      }
      else {
        (onSuccess || noop)(data.uniqueId);
        //console.log("INSERT INTO pending_messages (affectedRows: " + result.affectedRows + ", unique id: " + data.uniqueId + ")");
      }
    });
  }

  countPendingMsgs(userAppId, onSuccess, onError) {
    //console.log("countPendingMsgs userAppId", userAppId);
    let qryStr = "SELECT COUNT(uniqueId) as total FROM pending_messages WHERE receiverAppId = ? AND receivedAt IS NULL";

    conn.query(qryStr, [userAppId], function (error, result) {
      if (error) {
        console.error("DB::getAllPendingMsg: SELECT COUNT(uniqueId) as total FROM pending_messages WHERE receiverId = %s AND receivedAt IS NULL", userAppId, error.sqlMessage);
        (onError || noop)(error.sqlMessage);
      }
      else {
        (onSuccess || noop)(result[0]/*{total:number}*/);
        //console.log(result)
      }
    });
  }

  getPendingMsgs(userAppId, onSuccess, onError) {
    //console.log("countPendingMsgs userAppId", userAppId);
    let qryStr = "SELECT * FROM pending_messages WHERE receiverAppId = ? AND receivedAt IS NULL";

    conn.query(qryStr, [userAppId], function (error, results) {
      if (error) {
        console.error("DB::getPendingMsgs: SELECT * FROM pending_messages WHERE receiverId = %s AND receivedAt IS NULL", userAppId, error.sqlMessage);
        (onError || noop)(error.sqlMessage);
      }
      else {
        (onSuccess || noop)(results/*[{any}]*/);
        //console.log(result)
      }
    });
  }

  getAllPendingMsg(userAppId, onSuccess, onError) {
    let qryStr = 'SELECT * FROM pending_messages WHERE receiverAppId = ? AND receivedAt IS NULL';

    conn.query(qryStr, [userAppId], function (error, result) {
      if (error) {
        console.error("DB::getAllPendingMsg: SELECT * FROM pending_messages WHERE receiverId = %s AND receivedAt IS NULL", userAppId, error.sqlMessage);
        (onError || noop)(error.sqlMessage);
      }
      else {
        (onSuccess || noop)(result.insertId);
      }
    });
  }

  /**
   *
   * @param {{receiverAppId: receiverAppId, receivedAt:receivedAt}} data
   * @param onSuccess
   * @param onError
   */
  ackPendingMsgs(data, onSuccess, onError) {
    let qryStr = 'UPDATE pending_messages SET receivedAt = ? WHERE receiverAppId = ?';

    conn.query(qryStr, [data.receivedAt, data.receiverAppId], function (error, result) {
      if (error) {
        console.error("DB::ackPendingMsgs: UPDATE pending_messages SET receivedAt = %s WHERE msgId = %s", data.receivedAt, data.receiverAppId, error.sqlMessage);
        (onError || noop)(error.sqlMessage);
      }
      else {
        (onSuccess || noop)(result.affectedRows/*<number>*/);
        //console.log("UPDATE pending_messages affectedRows", result.affectedRows);
      }
    });
  }

  cleanPendingMsgs(senderId, onSuccess, onError) {
    conn.query(
      'SELECT uniqueId, receivedAt FROM pending_messages WHERE senderAppId = ? AND receivedAt IS NOT NULL',
      [senderId],
      function (error, result) {
        if (error) {
          console.error(
            "DB::cleanPendingMsgs: SELECT uniqueId, receivedAt FROM pending_messages WHERE senderAppId = %s AND receivedAt IS NOT NULL",
            senderId, error.sqlMessage
          );
          (onError || noop)(error.sqlMessage);
        }
        else {
          //console.log("(OK) cleanPendingMsgs QRY 1/2");
          let
            deletedRows = [],
            totalErrors = 0,
            numRows = result.length;

          result.forEach(function (row, idx) {
            //console.log(row)
            conn.query('DELETE FROM pending_messages WHERE uniqueId = ?', [row.uniqueId], function (error2, result2) {
              if (error2) {
                console.error(
                  "DB::cleanPendingMsgs: DELETE FROM pending_messages WHERE msgId = %s",
                  row.uniqueId, error.sqlMessage
                );
                totalErrors++;
              } else
                deletedRows.push({uniqueId: row.uniqueId, receivedAt: row.receivedAt});


              if (idx === numRows - 1) {
                if(totalErrors === numRows)(onError || noop)(error.sqlMessage);
                else{
                  (onSuccess || noop)(deletedRows/*<[{uniqueId:number, receivedAt:string}]>*/);
                  //console.log("DELETE pending_messages QRY 2/2", deletedRows);
                }
              }
            });
          })
        }//<-- Qry 1/" OK
      });
  }
}


module.exports = {DB};

/**
 * @typedef {Object} DBResponse
 * {
 * fieldCount: number,
 * affectedRows: number,
 * insertId: number,
 * serverStatus: number,
 * warningCount: number,
 * message: string,
 * protocol41: boolean,
 * changedRows: number
 * }
 */