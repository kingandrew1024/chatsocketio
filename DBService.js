[{}]

var noop = function () {
  //empty function :)
};

//------------ MYSQL SECTION --------
var mongoose = require('mongoose');

var pendingSchema, messageSchema;
var Pending, Message;

//============ END MYSQL SECTION ============

class DB {
  constructor() {
    mongoose.connect('mongodb://localhost/pending_messages', {useNewUrlParser: true});

    pendingSchema = mongoose.Schema({
      uniqueId: String,
      senderAppId: Number,
      groupId: Number,
      senderType: Number,
      receiverAppId: Number,
      text: String,
      fileType: String,
      fileSrc: String,
      fileMimeType: String,
      extraData: String,
      createdAt: String,
      processedAt: String,
      receivedAt: String,
      receiveStatus: Number
    });

    Pending = mongoose.model("Pending", pendingSchema);

    messageSchema = mongoose.Schema({
      uniqueId: String,
      senderAppId: Number,
      groupId: Number,
      senderType: Number,
      receiverAppId: Number,
      text: String,
      fileType: String,
      fileSrc: String,
      fileMimeType: String,
      extraData: String,
      createdAt: String,
      processedAt: String,
      receivedAt: String,
      receiveStatus: Number
    });

    Message = mongoose.model("Message", messageSchema);
  }

  /**
   *
   * @param {Message} data
   * @param onSuccess {function(string):*} - the uniqueId
   * @param onError
   */
  saveMessage(data, onSuccess, onError) {
    var newMessage = new Message({
      uniqueId: data.uniqueId,
      senderAppId: data.senderAppId,
      groupId: data.groupId,
      senderType: data.senderType,
      receiverAppId: data.receiverAppId,
      text: data.text,
      fileType: data.fileType,
      fileSrc: data.fileSrc,
      fileMimeType: data.fileMimeType,
      extraData: data.extraData,
      createdAt: data.createdAt,
      processedAt: data.processedAt,
      receivedAt: data.receivedAt,
      receiveStatus: 0
    });

    newMessage.save(function (error, response) {
      if (error) {
        (onError || noop)(error);
        console.error(getLogDate() + "saveMessage sender: %s, receiver: %s ", data.senderAppId, data.receiverAppId, error);
      }
      else {
        (onSuccess || noop)(response.uniqueId);
        //console.log(getLogDate()+"saveMessage uniqueId", response);
      }
    })
  }

  /**
   *
   * @param {Message} data
   * @param onSuccess {function(string):*} - the uniqueId
   * @param onError
   */
  savePendingMsg(data, onSuccess, onError) {
    var newPending = new Pending({
      uniqueId: data.uniqueId,
      senderAppId: data.senderAppId,
      groupId: data.groupId,
      senderType: data.senderType,
      receiverAppId: data.receiverAppId,
      text: data.text,
      fileType: data.fileType,
      fileSrc: data.fileSrc,
      fileMimeType: data.fileMimeType,
      extraData: data.extraData,
      createdAt: data.createdAt,
      processedAt: data.processedAt,
      receivedAt: data.receivedAt,
      receiveStatus: 0
    });

    newPending.save(function (error, response) {
      if (error) {
        (onError || noop)(error);
        console.error(getLogDate() + "savePendingMsg sender: %s, receiver: %s ", data.senderAppId, data.receiverAppId, error);
      }
      else {
        (onSuccess || noop)(response.uniqueId);
        //console.log(getLogDate()+"savePendingMsg uniqueId", response.uniqueId);
      }
    })
  }

  /**
   *
   * @param userAppId {number}
   * @param onSuccess {function(number):*}
   * @param onError
   */
  countPendingMsgs(userAppId, onSuccess, onError) {
    Pending.count({receiverAppId: userAppId, receivedAt: null}, function (error, count) {
      if (error) {
        (onError || noop)(error);
        console.error(getLogDate() + "countPendingMsgs userAppId: %s", userAppId, error);
      }
      else {
        (onSuccess || noop)(count);
        //console.log(getLogDate()+"countPendingMsgs", count);
      }
    })
  }

  /**
   *
   * @param userAppId
   * @param onSuccess {function(Array<Pending>):*}
   * @param onError
   */
  getPendingMsgsByUserId(userAppId, onSuccess, onError) {
    Pending.find({receiverAppId: userAppId, receivedAt: null}, function (error, response) {
      if (error) {
        (onError || noop)(error);
        console.error(getLogDate() + "getPendingMsgs userAppId: %s", userAppId, error);
      }
      else {
        (onSuccess || noop)(response);
        //console.log(getLogDate()+"getPendingMsgsByUserId uiserAppId: %s, total: %d", userAppId, response.length);
      }
    })
  }

  /**
   *
   * @param userAppId
   * @param onSuccess {function(Array<any>):*}
   * @param onError
   */
  getAllPendingMsg(userAppId, onSuccess, onError) {
    Pending.find(function (error, response) {
      if (error) {
        (onError || noop)(error);
        console.error(getLogDate() + "getAllPendingMsg userAppId: %s", userAppId, error);
      }
      else {
        (onSuccess || noop)(response);
        //console.log(getLogDate()+"getAllPendingMsg", response);
      }
    })
  }

  getHistoricMsgsByUserId(userAppId, onSuccess, onError) {
    //Message.find({senderAppId: userAppId, receiverAppId: userAppId}, function (error, response) {
    Message
      .find()
      .or([{senderAppId: userAppId}, {receiverAppId: userAppId}])
      //.sort('processedAt')
      .exec(function (error, response) {
        if (error) {
          (onError || noop)(error);
          console.error(getLogDate() + "getHistoricMsgsByUserId userAppId: %s", userAppId, error);
        }
        else {
          var tsNow = new Date().toISOString();
          var total = response.length

          for (var i = 0; i < total; i++) {
            if (response[i].receivedAt == null) {
              response[i].receivedAt = tsNow;
            }

            //this didn't work :(
            //delete response[i].__v;
            //delete response[i]._id;
          }

          if (total) {
            Message.updateMany({
              receiverAppId: userAppId,
              receivedAt: null
            }, {receivedAt: tsNow}, function (error, resp) {
              if (error) {
                console.error('getHistoricMsgsByUserId: cant update message.receivedAt', error)
              }
              /*else {
                console.log(getLogDate() + 'getHistoricMsgsByUserId: message.receivedAt updated to %s', tsNow)
              }*/
            });
          }

          (onSuccess || noop)(response);
          //console.log(getLogDate()+"getHistoricMsgsByUserId", response);
        }
      })
  }

  /**
   *
   * @param {{receiverAppId: number, receivedAt:string}} data
   * @param onSuccess {function(number):*} - num affected
   * @param onError
   */
  ackPendingMsgs(data, onSuccess, onError) {
    Pending.updateMany({receiverAppId: data.receiverAppId}, {receivedAt: data.receivedAt},
      /**
       *
       * @param error
       * @param response {{ n: number, nModified: number, ok: number }}
       */
      function (error, response) {
        if (error) {
          (onError || noop)(error);
          console.error(getLogDate() + "ackPendingMsgs receiverAppId: %s", data.receiverAppId, error);
        }
        else {
          (onSuccess || noop)(response.nModified);
          //console.log(getLogDate()+"ackPendingMsgs numAffected", response)
        }
      })
  }

  /**
   * Cleans the pending messages that have been succesfully delivered and updated by receiver
   * @param senderId {number}
   * @param onSuccess  {function(Array<{_id:string, uniqueId:string}>):*}
   * @param onError
   */
  cleanPendingMsgs(senderId, onSuccess, onError) {
    var deletedRows = [];
    Pending.find({senderAppId: senderId, receivedAt: {$ne: null}}, 'uniqueId', function (error, response1) {
      if (error) {
        (onError || noop)(error);
        console.error(getLogDate() + "cleanPendingMsgs.find() userAppId: %s", senderId, error);
      }
      else {
        deletedRows = response1;
        //console.log(getLogDate()+"cleanPendingMsgs:find", deletedRows);

        Pending.deleteMany({senderAppId: senderId, receivedAt: {$ne: null}},
          /**
           * @param error {*}
           * @param response2 {{ n: number, ok: number }} - n = num rows deleted, ok = {1|0}
           */
          function (error, response2) {
            if (error) {
              (onError || noop)(error);
              console.error(getLogDate() + "cleanPendingMsgs.deleteMany() userAppId: %s", senderId, error);
            }
            else {
              (onSuccess || noop)(deletedRows);
              //console.log(getLogDate()+"Delete many response:", response2)
            }
          });
      }
    });
  }

  /**
   * cleans the pending messages to be delivered to userAppId
   */
  clearPendingMsgsForUser(userAppId, onSuccess, onError) {
    Pending.deleteMany({receiverAppId: userAppId},
      /**
       * @param error {*}
       * @param response2 {{ n: number, ok: number }} - n = num rows deleted, ok = {1|0}
       */
      function (error, response2) {
        if (error) {
          (onError || noop)(error);
          console.error(getLogDate() + "clearPendingMsgsForUser.deleteMany() userAppId: %s", userAppId, error);
        }
        else {
          (onSuccess || noop)(response2);
          console.log(getLogDate() + "clearPendingMsgsForUser: Delete many response:", response2)
        }
      });
  }
}

function getLogDate() {
  return "[" + new Date().toLocaleString() + "] DB:: "
}


module.exports = {DB};
